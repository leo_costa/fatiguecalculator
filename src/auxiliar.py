import math
import matplotlib.pyplot as plt

def sign(value):
    try:
        val = math.ceil(value/abs(value))
    except ZeroDivisionError:
        return -1
    else:
        return val

def nextChar(char):
    return chr(ord(char) + 1)

def previousChar(char):
    return chr(ord(char) - 1)