import openpyxl as pyxl
import auxiliar
import re

class ExcelData(object):
    """
    Classe utilizada para extrair dados de uma planilha de excel.
    """
    def __init__(self, excel_filename):
        self.wbook = pyxl.Workbook()
        self.wsheet = self.wbook.active
        try:
            self.wbook = pyxl.load_workbook(excel_filename)
        except FileNotFoundError as identifier:
            print('Error: ', identifier)
        else:
            self.wsheet = self.wbook[self.wbook.sheetnames[0]]
            print("Planilha: ", self.wbook.sheetnames[0], "carregada!\n")


    def createCellName(self, column, row):
        """
        Cria o nome da célula com a coluna e linha fornecida.
        """
        return "{}{}".format(column, row)


    def findParam(self, paramName):
        """
        Retorna a coluna e linha do parâmetro fornecido.
        """
        for row in range(1, self.wsheet.max_row + 1):
            for column in "ABCDEFGHIJKLMNOP":
                cellName = self.createCellName(column, row)
                
                if self.wsheet[cellName].value == paramName:
                    return [column, row]
        raise ValueError("Parameter not found!")


    def getSingleParameterValue(self, paramName):
        """
        Retorna um único valor do parâmetro fornecido.
        """
        paramCell = self.findParam(paramName)
        cellName  = self.createCellName(paramCell[0], paramCell[1] + 1)
        return self.wsheet[cellName].value


    def setSingleParameterValue(self, paramName, paramValue):
        """
        Escreve na planilha o valor de um único parâmetro.
        """
        paramCell = self.findParam(paramName)
        cellName  = self.createCellName(paramCell[0], paramCell[1] + 1)
        self.wsheet[cellName] = paramValue


    def getMultipleParameterValues(self, paramName, count, v_offset = 1):
        """
        Retorna 'count' valores do parâmetro fornecido.
        """
        paramCell     = self.findParam(paramName)
        paramCell[1] += 1 + v_offset
        cellName      = self.createCellName(paramCell[0], paramCell[1])

        values = []
        for i in range(0, count):
            values.append(self.wsheet[cellName].value)

            paramCell[0] = auxiliar.nextChar(paramCell[0])
            cellName = self.createCellName(paramCell[0], paramCell[1])
        return values


    def setMultipleParameterValues(self, paramName, paramValues, v_offset = 1):
        """
        Escreve na planilha diversos valores do parâmetro fornecido.
        """
        paramCell     = self.findParam(paramName)
        paramCell[1] += 1 + v_offset
        cellName      = self.createCellName(paramCell[0], paramCell[1])

        for i in range(0, len(paramValues)):
            self.wsheet[cellName] = paramValues[i]

            paramCell[0] = auxiliar.nextChar(paramCell[0])
            cellName = self.createCellName(paramCell[0], paramCell[1])


    def getXMarkedOption(self, paramName):
        """
        Retorna a opção marcada com 'X' do parâmetro fornecido.
        """
        paramCell     = self.findParam(paramName)
        paramCell[1] += 1
        paramCell[0]  = auxiliar.nextChar(paramCell[0])
        cellName      = self.createCellName(paramCell[0], paramCell[1])

        while self.wsheet[cellName].value == '-' or self.wsheet[cellName].value != 'X':
            paramCell[1] += 1
            cellName = self.createCellName(paramCell[0], paramCell[1])

        if self.wsheet[cellName].value != None:
            paramCell[0] = auxiliar.previousChar(paramCell[0])
            cellName = self.createCellName(paramCell[0], paramCell[1])
            return self.wsheet[cellName].value
        raise ValueError("X Mark Not Found!")

    def getExcelTableValues(self, paramName, lastColumn, maxRow = 10):
        """
        Retorna todos os valores da tabela de valores que vai até a coluna 'lastColumn'.

        A tabela de valores deve ter seu fim indicado por um 'X'. A função irá avançar até 'maxRow'
        linhas a partir da linha inicial da tabela se não encontrar o caractere 'X'.
        """
        initCell     = self.findParam(paramName)
        initCell[1] += 1
        currentCell  = []
        currentCell.extend(initCell)

        cellName = self.createCellName(currentCell[0], currentCell[1])

        values = []

        while self.wsheet[cellName].value != 'X' and currentCell[1] <= initCell[1] + maxRow:
            values.append(self.wsheet[cellName].value)

            currentCell[0] = auxiliar.nextChar(currentCell[0])

            if currentCell[0] > lastColumn:
                currentCell[0] = initCell[0]
                currentCell[1] += 1
            
            cellName = self.createCellName(currentCell[0], currentCell[1])
        return values


    def saveExcelSheet(self, filename):
        """
        Salva a planilha atual com o nome fornecido.
        """
        self.wbook.save(filename)

if __name__ == "__main__":
    excelData = ExcelData('PlanilhaElemaq2_Fadiga.xlsx')
    param = excelData.getExcelTableValues("Pontos do Diagrama de Rainflow", "P")
    print("Dados", param)
