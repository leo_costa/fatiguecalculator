import matplotlib.pyplot as plt
import math
import os
import strains
import re
from statistics import mean

import auxiliar
import ExcelData as edata

def annotateChar(point, char):
    """
    Desenha um caractere no gráfico no ponto definido.
    """
    plt.annotate(char,
                    xy=(point[0], point[1] + auxiliar.sign(point[1])*2),
                    rotation=0,
                    va='center',
                    ha='center',
                    color='red'
                    )

def annotateChars(points, chars, plot):
    """
    Desenha os caracteres fornecidos nos pontos do gráfico.
    """
    for i,char in enumerate(chars):
        plot.annotate(char,
                    xy=(points[0][i], points[1][i] + auxiliar.sign(points[1][i])*2),
                    rotation=0,
                    va='center',
                    ha='center',
                    color='red'
                    )

def drawArrow(x1, x2, y, text):
    """
    Desenha uma flecha horizontal na altura y, de x1 a x2, com o texto fornecido em cima.
    """
    # Annotate arrow
    plt.annotate('',
                xy=(x1, y),
                xytext=(x2, y),
                xycoords='data',
                arrowprops=dict(arrowstyle='<->', connectionstyle='arc3', color='black', lw=2)
                )

    # Annotate Text
    plt.annotate(text, # text to display
                xy=(mean([x1,x2])-1, y*0.95),
                rotation=0,
                va='center',
                ha='left',
                )


class Rainflow(object):
    """
    Classe responsável pela decomposição de Rainflow.
    """
    def __init__(self, excelFilename):
        self.rainflowPoints = [] # Pontos
        self.startEndPoints = [] # Pontos A e A'
        self.pointLabels    = [] # Letras dos pontos
        self.startIndex     = 0  # Índice inicial da decomposição (posição do A)
        self.endIndex       = 0  # Índice final da decomposição (posição do A')
        self.rainflowTable  = [] # Tabela com a decomposição de Rainflow
        self.maxY           = 0  # Valor máximo em Y
        self.currentPattern = (0,0,0) # Início do primeiro padrão, fim do primeiro padrão e fim do segundo padrão
        self.stressConfig   = strains.Stress(excelFilename) # Armazena o tipo de esforço do gráfico de Rainflow.

    def plotRainflowPoints(self):
        """
        Plota o gráfico de Rainflow com os pontos lidos.
        """
        plt.plot(range(0, len(self.rainflowPoints)), self.rainflowPoints, marker='o', markersize=6)
        plt.title(self.stressConfig.sdict[self.stressConfig.sType][1] + " x Tempo")
        plt.xlabel("Tempo")
        plt.ylabel( self.stressConfig.sdict[self.stressConfig.sType][1] + " [" + self.stressConfig.sdict[self.stressConfig.sType][0] + "]")

        self.maxY = max(self.rainflowPoints)*1.20
        plt.ylim(-self.maxY, self.maxY)
        plt.xticks(list(range(0,len(self.rainflowPoints))), list(range(0,len(self.rainflowPoints))))

        plt.grid(True)
        plt.subplots_adjust(left=0.13, bottom=0.10, right=0.99, top=0.95)
        plt.show(block = False)


    def getUnit(self):
        """
        Lê a unidade/tipo de esforço escolhida na planilha.
        """
        unit = re.split("[()]", self.stressConfig.excelData.getXMarkedOption("Tipo de Esforço"))
        if list(self.stressConfig.sdict.keys()).count(unit[1]) == 0:
            raise ValueError("Undefined Stress Type!")
        self.stressConfig.sType = unit[1]


    def defineRainflowChart(self):
        """
        Lê os pontos do diagrama de Rainflow da planilha.
        """
        self.getUnit()
        data = self.stressConfig.excelData.getExcelTableValues("Pontos do Diagrama de Rainflow", "P")
        self.rainflowPoints = data
            
        print("\nQuando o gráfico aparecer, identifique os pontos A e A' clicando neles")
        self.plotRainflowPoints()


    def getStartEndPoints(self):
        """
        Recebe e mostra os pontos inicial e final da decomposição de Rainflow.
        """
        self.pointLabels = []
        self.startEndPoints = list(plt.ginput(2, timeout=0))
        aux = ['A',"A'"]
        for i,point in enumerate(self.startEndPoints):
            point = (round(point[0]), self.rainflowPoints[int(point[0])])
            self.startEndPoints[i] = point
            annotateChar(point, aux[i])
        plt.pause(0.5)
        plt.show(block = False)


    def defineLabels(self):
        """
        Insere as respectivas letras para cada ponto entre A e A'
        """
        self.getStartEndPoints()

        currentLabel = 'A'

        self.startIndex = int(self.startEndPoints[0][0])
        self.endIndex   = int(self.startEndPoints[1][0])

        for i in range(self.startIndex, self.endIndex):
            annotateChar((i,self.rainflowPoints[i]), currentLabel)
            self.pointLabels.append(currentLabel)
            currentLabel = auxiliar.nextChar(currentLabel)

        self.pointLabels.append("A'")
        plt.pause(0.5)
        plt.show(block = False)


    def findPatterns(self):
        """
        Tenta encontrar dois padrões de repetição (n e n+1) no diagrama de Rainflow
        """
        rootPair           = (0, self.rainflowPoints[0])
        searchStart        = 0
        secondPatternStart = 0

        while rootPair[0] < len(self.rainflowPoints)-1:
            firstPatternStart = rootPair[0]
            try:
                n1_Index = self.rainflowPoints.index(rootPair[1], searchStart+1)
            except ValueError:
                # Procura o padrão a partir do próximo valor
                rootPair = (rootPair[0]+1, self.rainflowPoints[rootPair[0]+1])
                searchStart += 1
            else:
                searchStart = n1_Index
                secondPatternStart = searchStart

                while secondPatternStart < len(self.rainflowPoints) and \
                        self.rainflowPoints[firstPatternStart] == self.rainflowPoints[secondPatternStart] and \
                        firstPatternStart < searchStart:
                    firstPatternStart +=1
                    secondPatternStart +=1

                if firstPatternStart == searchStart: # Encontrou dois padrões
                    # Retorna o inicio do primeiro padrão
                    # o fim dele, e o fim do segundo padrão
                    return (rootPair[0], searchStart, secondPatternStart)

        return (0,0,0) # Não foi possível encontrar nenhum padrão


    def showPatterns(self):
        """
        Indica no gráfico de Rainflow os padrões de repetição encontrados.
        """
        if self.currentPattern == (0,0,0):
            patternIndex = self.findPatterns()
        else:
            patternIndex = self.currentPattern

        if patternIndex != (0,0,0):
            self.currentPattern = patternIndex
            drawArrow(patternIndex[0], patternIndex[1], -max(self.rainflowPoints)*1.10, 'n')
            drawArrow(patternIndex[1], patternIndex[2], -max(self.rainflowPoints)*1.10, 'n+1')
        else:
            print('\n\tErro! Não foi possível encontrar nenhum padrão n e n+1 nos ciclos!')


    def plotRainflowCharts(self, pointsY, pointsX, labels, nPlots, currPlot):
        """
        Desenha o gráfico de Rainflow atualizado após remover um intervalo.
        """
        if currPlot == 0:
            self.rainflowFigure = plt.figure()
            plt.suptitle("Decomposição por Rainflow")
            rows = math.ceil((nPlots)/2)
            self.rainflowPlots = [ self.rainflowFigure.add_subplot(rows, 2, i+1) for i in range(0, nPlots)]
            plt.subplots_adjust(left=0.10, bottom=0.06, right=0.98, top=0.90)
        
        self.rainflowPlots[currPlot].plot(pointsX, pointsY)
        self.rainflowPlots[currPlot].grid(True)
        self.rainflowPlots[currPlot].set_ylim(-self.maxY, self.maxY)
        xTicks = list(range(self.startIndex,self.endIndex))
        self.rainflowPlots[currPlot].set_xticks(xTicks)

        annotateChars((pointsX, pointsY), labels, self.rainflowPlots[currPlot])
        plt.show(block = False)


    def rainflowAlgorithm(self):
        """
        Executa o algoritmo de Rainflow.
        """
        self.rainflowTable = []
        points = self.rainflowPoints[self.startIndex:self.endIndex+1]
        xVal   = list(range(self.startIndex, self.endIndex+1))
        labels = self.pointLabels
        totalPairs = round((len(points)-2)/2)
        if len(points)%2 == 1:
            totalPairs += 1
        pairsFound = 0

        while len(points) > 2:
            currentPos = 0
            while abs(points[currentPos] - points[currentPos+1]) >\
                    abs(points[currentPos+1] - points[currentPos+2]):
                currentPos = currentPos + 1 # AB > BC, continua

                if currentPos+2 == len(points):
                    print("\n\tErro! Escolha incorreta dos pontos A e A', o resultado apresentado não está correto!")
                    return

            # AB <= BC
            # Salva AB
            maxVal = max(points[currentPos:currentPos+2])
            minVal = min(points[currentPos:currentPos+2])
            ampVal = abs(points[currentPos] - points[currentPos+1])
            maxStrain = self.stressConfig.calcStrain(maxVal)
            minStrain = self.stressConfig.calcStrain(minVal)
            self.rainflowTable.append((labels[currentPos]+labels[currentPos+1],
                                        maxVal,
                                        minVal,
                                        ampVal,
                                        maxStrain,
                                        minStrain))
            # Remove AB
            del points[currentPos:currentPos+2]
            del xVal[currentPos:currentPos+2]
            del labels[currentPos:currentPos+2]

            self.plotRainflowCharts(points, xVal, labels, totalPairs, pairsFound)
            pairsFound += 1
        print("\n\t Decomposição realizada com sucesso!")


    def saveRainflowTable(self):
        """
        Salva uma tabela com os dados da decomposição de Rainflow.
        """
        file = open(os.getcwd() + "/rainflowTable.csv", "w")
        text = ""
        unit = self.stressConfig.sdict[self.stressConfig.sType][0]
        print("\nCiclo Max [{0}] Min [{0}] Amp. [{0}] T. Max [MPa] T. Min [MPa]\n".format(unit))
        text += "Ciclo;Max[{0}];Min[{0}];Amplitude[{0}];TensãoMax[MPa];TensãoMin[MPa]\n".format(unit)

        for row in self.rainflowTable:
            for val in row:
                print("{0}\t|".format(val), end='')
                text += "{0};".format(val)
            print('\n', end='')
            text += "\n"

        file.write(text)
        file.close()


    def solveRainflow(self):
        """
        Executa os passos necessários para realizar a decomposição por Rainflow.
        """
        plt.close('all')
        plt.clf()
        self.defineRainflowChart()
        self.showPatterns()
        self.defineLabels()

        correct = input("\nAs letras estão posicionadas corretamente? Y/N ")
        while correct.lower() != 'y':
            plt.clf()
            plt.pause(0.5)
            self.plotRainflowPoints()
            self.showPatterns()
            self.defineLabels()
            correct = input("\nAs letras estão posicionadas corretamente? Y/N ")
        plt.savefig(os.getcwd() + '/diagrama.png')

        if ['A','F'].count(self.stressConfig.sType) > 0: # Tensão normal
            self.stressConfig.setNormalStrainProp()
        elif ['C','T'].count(self.stressConfig.sType) > 0: #Tensão de cisalhamento
            self.stressConfig.setShearStrainProp()
        
        self.rainflowAlgorithm()
        plt.savefig(os.getcwd() + '/decompRainflow.png')

        print('\nResultado da decomposição\n')
        self.saveRainflowTable()
        plt.show()


if __name__ == "__main__":
    rf = Rainflow("PlanilhaElemaq2_Fadiga.xlsx")
    rf.solveRainflow()
    input("Pressione Enter para finalizar!")
