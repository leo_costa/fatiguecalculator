import rainflow
import strains
import ExcelData as edata

import math
from math import pi
# import pickle Para salvar objetos
import re

from sympy import Function, Symbol, solve
import sympy

from scipy.interpolate import interp1d
import numpy as np

# Incógnitas suportadas.
F, Q, M, T, Wf, Wt, b, h, d, n, S = sympy.symbols('F Q M T Wf Wt b h d n S')


def getSolution(answer):
    """
    Retorna a incógnita e sua(s) soluções.
    """
    symb = list(answer[0].keys())[0]
    if len(answer) == 1:
        return (symb, answer[0][symb])
    elif len(answer) > 1:
        return (symb, answer[0][symb], answer[1][symb])

class Fatigue():
    def __init__(self):
        self.SN     = 0.0
        #SigmaE , SigmaR ou TauE, TauR, a conversão é feita automaticamente
        self.Sigma  = {'E':0.0, 'R':0.0} 
        self.snExp =   {'S':[1400, 0.5, 700],
                        'F':[400 , 0.4, 160],
                        'A':[330 , 0.4, 130],
                        'C':[280 , 0.4, 100]}
        self.materialType = 'S'
        self.basquinEqCoefs = {'S':[3,0.81,2,1],
                            'A':[5.7,0.85,1.53,0.53]}
        self.M  = 0.0
        self.B  = 0.0
        self.Kf = 0.0
        self.loadType = 'S' #Simétrico
                        #'A' Assimétrico
                        #'R' Rainflow
        self.excelData = edata.ExcelData(
                                  "../excelSheets/PlanilhaElemaq2_Fadiga.xlsx")

        self.rainflowAlg = rainflow.Rainflow(
                                  "../excelSheets/PlanilhaElemaq2_Fadiga.xlsx")
        self.rainflowCoefs     = []
        self.accumulatedDamage = 0
        # Tensão alternada e média
        self.sigmaAM           = {'a': 0.0, 'm': 0.0} 
        self.critType          = 'S' #S-Soderberg R-Gerber G-Goodman
        self.lifePrevType      = 'B' #B-Basquin M-Morrow

        print(self.excelData.getSingleParameterValue("SN [MPa]"))


    def setSNCP(self):
        """
        Define o valor do SNcp com base nos seguintes critérios:
        - Verifica se o valor do SNcp foi definido no arquivo excel fornecido
        - Calcula o valor do SNcp de acordo com o material fornecido.
        """
        self.Sigma['E'] = self.excelData.getSingleParameterValue("SigmaE")
        self.Sigma['R'] = self.excelData.getSingleParameterValue("SigmaR")

        mat = self.excelData.getXMarkedOption("Tipo do Material")
        matList = re.split("[()]", mat)

        if list(self.snExp.keys()).count(matList[1]) == 0:
            raise ValueError("Undefined Material!")

        self.materialType = matList[1]
        if self.excelData.getSingleParameterValue("SNcp [MPa]") == None: 
            # Nesse caso, sn = sigmaR*x
            if self.Sigma['R'] <= self.snExp[self.materialType][0]:
                self.SN = self.snExp[self.materialType][1]*self.Sigma['R']
            else:
                self.SN = self.snExp[self.materialType][2]
        else: 
            self.SN = self.excelData.getSingleParameterValue("SNcp [MPa]")
        print("\n\tSNcp = {0:.2f} MPa".format(self.SN))


    def setSN(self):
        """
        Define o valor do SN com base nos seguintes critérios:
        - Verifica se o valor do SN foi definido no arquivo excel fornecido
        - Calcula o valor do SN de acordo com os coeficientes de correção.
        """
        if self.excelData.getSingleParameterValue("SN [MPa]") == None:
            self.setSNCP()
            coefs = self.excelData.getMultipleParameterValues( \
                                                 "Coeficientes de Correção", 6)
            if max(coefs) > 1 or min(coefs) <= 0:
                raise ValueError('Coeficiente > 1 ou <= 0. Corrija!')
                
            for coef in coefs:
                self.SN *= coef
            self.excelData.setSingleParameterValue("SN [MPa]", self.SN)
        else:
            self.SN = self.excelData.getSingleParameterValue("SN [MPa]")
        print("\tSN = {0:.2f} MPa".format(self.SN))


    def setMB(self):
        """
        Calcula o valor dos coeficientes de Basquin.
        """
        if list(self.basquinEqCoefs.keys()).count(self.materialType) > 0:
            den  = self.basquinEqCoefs[self.materialType][0]
            num  = self.basquinEqCoefs[self.materialType][1]
            exp1 = self.basquinEqCoefs[self.materialType][2]
            exp2 = self.basquinEqCoefs[self.materialType][3]

            self.M = (1/den)*math.log10((0.9*self.Sigma['R'])/(self.SN))
            self.B = math.log10((num*(self.Sigma['R']**exp1))/(self.SN**exp2))
            print("\n\tCoeficientes de Basquin: " + \
                            "m = {0:.4f}, b = {1:.4f}".format(self.M, self.B))
            self.excelData.setSingleParameterValue("mx", self.M)
            self.excelData.setSingleParameterValue("bx", self.B)
        else:
            print("\n\tCoeficientes de Basquin não disponíveis para este " + \
                                                        "tipo de material!")


    def setKf(self):
        """
        Define o valor do Kf
        -Se ele foi deifnido diretamente no excel, não realiza nenhum cálculo
        -Caso contrário, calcula o valor do Kf com base no q e Kt
        """
        if self.excelData.getSingleParameterValue("ValorKf") != None:
            self.Kf = self.excelData.getSingleParameterValue("ValorKf")
        else:
            r  = self.excelData.getSingleParameterValue("rho")
            Kt = self.calcKt(r)

            q = 0
            if self.excelData.getSingleParameterValue("ValorQ") == None:
                rA = self.excelData.getSingleParameterValue("raiz_a")
                q = 1/(1 + (rA/math.sqrt(r)))
                self.excelData.setSingleParameterValue("ValorQ", q)
            else:
                q = self.excelData.getSingleParameterValue("ValorQ")
            self.Kf = q*(Kt -1) + 1

            self.excelData.setSingleParameterValue("ValorKf", self.Kf)
            self.excelData.setSingleParameterValue("ValorKt", Kt)
        print("\n\tKf = {0:.5f}".format(self.Kf))


    def calcKt(self, r):
        """
        Calcula o valor do Kt
        - Se ele foi definido diretamente no excel, não realiza nenhum cálculo
        - Caso contrário, calcula o valor do Kt com base no valor de A, b e D/d
        """
        Kt = 0
        Dd = []
        A  = []
        b  = []
        DdCurrent = 0
        d  = 0

        if self.excelData.getSingleParameterValue("ValorKt") != None:
            Kt = self.excelData.getSingleParameterValue("ValorKt")
        else:
            DdCurrent = self.excelData.getSingleParameterValue("D")
            d = self.excelData.getSingleParameterValue("d")

            DdCurrent = DdCurrent / d
            print("\n\tD/d = {:.2f}".format(DdCurrent))

            A  = self.excelData.getMultipleParameterValues("A", 2, 0)
            b  = self.excelData.getMultipleParameterValues("b", 2, 0)
            Dd = self.excelData.getMultipleParameterValues("Dd", 2, 0)

            print("\n\tDd = ", Dd, "A = ", A, "b = ", b)
            
            fA = interp1d(np.array(Dd), np.array(A))
            fb = interp1d(np.array(Dd), np.array(b))

            Kt = fA(DdCurrent)*pow(r/d,fb(DdCurrent))

        print("\tKt = {:.5f}".format(Kt))
        return Kt


    def setLoadType(self):
        """
        Extrai o tipo de carga do excel fornecido.
        """
        load = re.split("[()]", 
                self.excelData.getXMarkedOption("Tipo de Carregamento"))
        while ['S', 'A', 'R'].count(load[1]) == 0:
            raise ValueError("Undefined Load Type!")
        self.loadType = load[1]


    def getMinMaxStrains(self):
        """
        Extrai os esforços máximo e mínimo do excel fornecido.
        """
        sMax = eval(str(self.excelData.getSingleParameterValue("Máximo")))
        sMin = eval(str(self.excelData.getSingleParameterValue("Mínimo")))
        print("\t\t tMax = {0} | tMin = {1}".format(sMax, sMin))
        return (sMax, sMin)


    def convertSigmaTau(self):
        """
        Converte Sigma em Tau caso necessário
        """
        if ['S','C','T'].count(self.rainflowAlg.stressConfig.sType) > 0:
            if self.excelData.getSingleParameterValue("TauE") == None or \
                self.excelData.getSingleParameterValue("TauR") == None:

                # Converte Sigma para Tau
                self.Sigma['E'] /= math.sqrt(3)
                self.Sigma['R'] *= 0.8
                self.excelData.setSingleParameterValue("TauE", self.Sigma['E'])
                self.excelData.setSingleParameterValue("TauR", self.Sigma['R'])
            else:
                self.Sigma['E'] = self.excelData.getSingleParameterValue("TauE")
                self.Sigma['R'] = self.excelData.getSingleParameterValue("TauR")


    def calcStrains(self):
        """
        Calcula o esforço com base nos esforços máximos e mínimos ou na 
        decomposição por Rainflow.
        """
        if self.loadType == 'R':
            self.rainflowAlg.solveRainflow()
            self.convertSigmaTau()

        else:
            unit = re.split("[()]", self.excelData.getXMarkedOption(
                                                            "Tipo de Esforço"))

            if list(self.rainflowAlg.stressConfig.sdict.keys()).count(unit[1]) == 0:
                raise ValueError("Undefined Stress Type!")
            self.rainflowAlg.stressConfig.sType = unit[1]

            self.convertSigmaTau()

            s = self.getMinMaxStrains()
            self.sigmaAM['m'] = self.Kf*(s[0] + s[1])/2 
            self.sigmaAM['a'] = self.Kf*(s[0] - s[1])/2 
            print("\n\tSigmaA/TauA = ", self.sigmaAM['a'])
            print("\tSigmaM/TauM = ", self.sigmaAM['m'])

            self.excelData.setSingleParameterValue("SigmaM",
                    str(self.sigmaAM['m']))
            self.excelData.setSingleParameterValue("SigmaA",
                    str(self.sigmaAM['a']))


    def calcSafetyCoefficient(self):
        """
        Calcula o coeficiente de segurança caso ele seja a incógnita, ou,
        calcula o valor da incógnita do problema.
        """
        result = (0,0)
        if self.loadType == 'A' or self.loadType == 'R':
            critType = re.split("[()]" ,
                      self.excelData.getXMarkedOption("Critério de Segurança"))

            if ['S','R','G'].count(critType[1]) == 0:
                raise ValueError("Undefined Safety Criteria!")
            self.critType = critType[1]

            if self.loadType == 'A':
                result = self.calcASymmetricSafetyCoef(self.sigmaAM['a'],
                                                       self.sigmaAM['m'])
            elif self.loadType == 'R':
                self.calcRainflowSafetyCoef()

        elif self.loadType == 'S':
            result = self.calcSymmetricSafetyCoef()

        if ['A','S'].count(self.loadType) > 0:
            if result[0].name == 'n' and result[1] < 1:
                N = self.calcLifetime(self.sigmaAM['a'], self.sigmaAM['m'])
                print("Previsão de durabilidade = {0:.2f} ciclos".format(N))
                self.excelData.setSingleParameterValue("Número de Ciclos (N)",
                                                        N)
                self.excelData.setSingleParameterValue(
                              "Coeficiente de Segurança (n)", float(result[1]))
            else:
                res = [result[0].name, float(result[1])]
                self.excelData.setMultipleParameterValues("Incógnita", res, 0)

                if len(result) > 2:
                    res = [result[0].name, float(result[2])]
                    self.excelData.setMultipleParameterValues("Incógnita", res)


    def calcSymmetricSafetyCoef(self):
        """
        Calcula o coeficiente de segurança para cargas simétricas.
        """
        n = eval(str(self.excelData.getSingleParameterValue(
                                              "Coeficiente de Segurança (n)")))
        R = solve(self.sigmaAM['a'] - self.SN/n, dict=True)
        print(R)
        return getSolution(R)


    def calcASymmetricSafetyCoef(self, sigmaA, sigmaM):
        """
        Calcula o coeficiente de segurança para cargas assimétricas.
        """
        if self.critType == 'S':
            return self.calcSoderberg(sigmaA, sigmaM)
        elif self.critType == 'R':
            return self.calcGerber(sigmaA, sigmaM)
        elif self.critType == 'G':
            return self.calcGoodman(sigmaA, sigmaM)


    def calcSoderberg(self, sigmaA, sigmaM):
        """
        Calcula o coeficiente de segurança de Soderberg.
        """
        ns = eval(str(self.excelData.getSingleParameterValue(
                                              "Coeficiente de Segurança (n)")))
        
        R = solve(sigmaA/self.SN + sigmaM/self.Sigma['E'] - 1/ns, dict=True)
        print(R)
        return getSolution(R)


    def calcGerber(self, sigmaA, sigmaM):
        """
        Calcula o coeficiente de segurança de Gerber.
        """
        ng = eval(str(self.excelData.getSingleParameterValue(
                                              "Coeficiente de Segurança (n)")))

        R = solve(sigmaA/self.SN + (sigmaM/self.Sigma['R'])**2 - 1/ng,
                                                                     dict=True)
        print(R)
        return getSolution(R)

    def calcGoodman(self, sigmaA, sigmaM):
        """
        Calcula o coeficiente de segurança de Goodman.
        """
        sigmaEst = self.Sigma['R']*((self.Sigma['E'] - self.SN)/(self.Sigma['R'] - self.SN))
        print("\n\t\t Sigma*/Tau* = {0:.2f}".format(sigmaEst))
        self.excelData.setSingleParameterValue("Sigma*", sigmaEst)

        ng = eval(str(self.excelData.getSingleParameterValue(
                                              "Coeficiente de Segurança (n)")))
        
        if type(sigmaM) == float or type(sigmaM) == np.float64 or \
            type(sigmaM) == int:
            if sigmaM <= sigmaEst:
                R = solve(sigmaA/self.SN + sigmaM/self.Sigma['R'] - 1/ng,
                                                                     dict=True)
            elif sigmaM > sigmaEst:
                R = solve(sigmaA + sigmaM - self.Sigma['E']/ng, dict=True)
            print(R)
            return getSolution(R)
        else:
            print("\n\t***Checar a condição do Sigma* para saber " + \
                                                  "qual o resultado certo!***")
            R1 = solve(sigmaA/self.SN + sigmaM/self.Sigma['R'] - 1/ng,
                                                                     dict=True)
            R2 = solve(sigmaA + sigmaM - self.Sigma['E']/ng, dict=True)

            res1 = getSolution(R1)
            res2 = getSolution(R2)
            sM1 = sigmaM.subs(res1[0], res1[1])
            sM2 = sigmaM.subs(res2[0], res2[1])

            print("\t\t{0} <= {1} | {2} = {3}".format(sM1, sigmaEst, res1[0],
                                                                      res1[1]))
            print("\t\t{0} > {1} | {2} = {3}".format(sM2, sigmaEst, res2[0],
                                                                      res2[1]))

            if sM1 <= sigmaEst:
                return res1
            elif sM2 > sigmaEst:
                return res2

    def calcRainflowSafetyCoef(self):
        """
        Calcula o coeficiente de segurança para a decomposição em Rainflow.
        """
        rainflowStrains = self.rainflowAlg.rainflowTable
        accDamage = 0.0
        for i,cycle in enumerate(rainflowStrains):
            sM = self.Kf*(cycle[4] + cycle[5])/2
            sA = self.Kf*(cycle[4] - cycle[5])/2
            result  = self.calcASymmetricSafetyCoef(sA, sM)
            n = result[1]
            N = -1

            if n < 1: # Falha
                N = self.calcLifetime(sA, sM)
                accDamage += 1/N
            rainflowStrains[i] = cycle.__add__((sM, sA, n, N))
        
        self.accumulatedDamage = accDamage
        self.rainflowCoefs = rainflowStrains
        print("\tCiclo\tMax\tMin\tAmp\tTmax\tTmin\tsM\tsA\tn\tPrev.Vida")
        for row in rainflowStrains:
            for val in row:
                if type(val) != str:
                    print("\t{0:.2f} ".format(val), end='')
                else:
                    print("\t{0} ".format(val), end='')
            print('')

        print("\t\tDano acumulado = {0} dano/ciclo de carga".format(accDamage))
        self.excelData.setSingleParameterValue("Dano Acumulado", accDamage)
        print("\t\tDurabilidade estimada = {0:.2f} ciclos".format(1/accDamage))
        self.excelData.setSingleParameterValue("Durabilidade Estimada",
                                                                   1/accDamage)


    def setLifetimeType(self):
        """
        Define o tipo de modelo de previsão de vida.
        """
        lifeType = re.split("[()]", self.excelData.getXMarkedOption(
                                                           "Previsão de Vida"))

        if ['B', 'M'].count(lifeType[1]) == 0:
            raise ValueError("Undefined Life Prediction Method!")
        self.lifePrevType = lifeType[1]


    def calcBasquinLifetime(self, sA, sM):
        """
        Calcula a previsão de vida pelo modelo de Basquin.
        """
        sigmaA = 0
        if self.critType == 'S' :
            sigmaA = (sA*self.Sigma['E'])/(self.Sigma['E'] - sM)
            self.excelData.setSingleParameterValue("Sigma Soderberg", sigmaA)
        elif self.critType == 'G' or self.critType == 'R':
            sigmaA = (sA*self.Sigma['R'])/(self.Sigma['R'] - sM)
            self.excelData.setSingleParameterValue("Sigma Goodman", sigmaA)
        
        print("\t\tTAE = {:.3f}".format(sigmaA))
        N = (pow(10, self.B/self.M)) / (pow(sigmaA, 1/self.M))
        return N


    def calcMorrowLifetime(self, sA, sM):
        """
        Calcula a previsão de vida pelo modelo de Morrow.
        """
        N = pow((pow(10, self.B) - sM*pow(2, -self.M) ) / sA, 1/self.M)
        return N


    def calcLifetime(self, sA, sM):
        """
        Calcula a previsão de vida.
        """
        if self.lifePrevType == 'B':
            return self.calcBasquinLifetime(sA, sM)
        elif self.lifePrevType == 'M':
            return self.calcMorrowLifetime(sA, sM)


if __name__ == "__main__":
    fadiga = Fatigue()

    fadiga.setSN()
    fadiga.setMB()
    fadiga.setKf()
    fadiga.setLoadType()
    fadiga.calcStrains()
    fadiga.setLifetimeType()
    fadiga.calcSafetyCoefficient()
    fadiga.excelData.saveExcelSheet("../excelSheets/Resultado.xlsx")
