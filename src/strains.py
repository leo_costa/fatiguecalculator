import ExcelData as edata

class Stress():
    def __init__(self, excelFilename):
        self.sdict =   {'N':['MPa', 'Tensão Normal'      ], \
                        'A':['N'  , 'Forca Axial'        ], \
                        'F':['Nm' , 'Momento Fletor'     ], \
                        'S':['MPa', 'Tensão Cisalhamento'], \
                        'C':['N'  , 'Forca Cortante'     ], \
                        'T':['Nm' , 'Torque'             ]}
        self.sType = 'N'

        self.normalStrainProp = 0 # Area ou modulo de resistencia a flexão
        self.shearStrainProp  = 0 # Area ou modulo de resistencia a torção
        self.excelData = edata.ExcelData(excelFilename)


    def setNormalStrainProp(self):
        matProperty = "Area N [mm2]"

        if self.sType == 'F':
            matProperty = "Mod. Res. Flexão [mm3]"

        self.normalStrainProp = self.setStrainProp(matProperty)


    def setShearStrainProp(self):
        matProperty = "Area C [mm2]"

        if self.sType == 'T':
            matProperty = "Mod. Res. Torção [mm3]"

        self.shearStrainProp = self.setStrainProp(matProperty)


    def setStrainProp(self, prop):
        val = self.excelData.getSingleParameterValue(prop)
        return val


    def calcStrain(self, val):
        if ['A','F'].count(self.sType) != 0:
            return self.normalStrain(val)
        elif ['C','T'].count(self.sType) != 0:
            return self.shearStrain(val)
        return val


    def normalStrain(self, val):
        return (val/self.normalStrainProp)


    def shearStrain(self, val):
        return (val/self.shearStrainProp)


if __name__ == "__main__":
    pass
