# Python Fatigue Calculator

To use this software you only need to fulfill the parameters in the 
[excel model sheet](../master/excelSheets/PlanilhaElemaq2_Fadiga.xlsx) and run the software:

```bash
python3 fatigue.py
```

The result will be saved in a sheet called _**Resultado.xlsx**_.

# Features

The software currently supports:

* Rainflow decomposition
* Symmetric loads
* Assimetric loads
* Basquin and Morrow life prevision models
* Soderberg, Gerber and Goodman safety coefficients
